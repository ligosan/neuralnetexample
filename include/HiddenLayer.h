#include <vector>
#include <cmath>

#include "Layer.h"
#include "OutputLayer.h"

using namespace std;

#ifndef HIDDEN_LAYER
#define HIDDEN_LAYER

namespace NeuralNet {
    class HiddenLayer : public Layer {
        public:
            HiddenLayer(int numberOfInputNeurons, int numberOfNeurons);

            void BackPropagation(NeuralNet::Layer& outputLayer, NeuralNet::Layer& inputLayer);

        private:
    };
}

#endif