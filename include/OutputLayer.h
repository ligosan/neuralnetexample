#include <vector>
#include <cmath>


#include "Layer.h"

using namespace std;

#ifndef OUTPUT_LAYER
#define OUTPUT_LAYER

namespace NeuralNet {
    class OutputLayer : public Layer {
        public:
            OutputLayer(int numberOfInputNeurons, int numberOfNeurons);

            vector<int>& GetOutputValues();
            int GetOutputValueByIndex(int index);
            double CalculateError(int expectedOutput);
            int GetHighestConfidence();

            int GetDesiredOutput();

            void BackwardPropagation(vector<double>& inputValues);

        private:
            vector<int> _outputValues;
            int _desiredOutput;
    };
}

#endif