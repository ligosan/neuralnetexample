#include "Layer.h"

#include "InputLayer.h"
#include "HiddenLayer.h"
#include "OutputLayer.h"

using namespace std;

namespace NeuralNetwork {
    class NeuralNetwork {
        public:
            NeuralNetwork(NeuralNet::InputLayer& inputLayer, NeuralNet::OutputLayer& outputLayer);
            NeuralNetwork();

            void Train();
            void Inference();

            void SaveWeights();
            void LoadWeights();

            void LoadInputValues(vector<double>& inputValues);

            void GenerateRandomWeights();

            void AddHiddenLayer(NeuralNet::HiddenLayer& hiddenLayer);
            vector<NeuralNet::HiddenLayer*>& GetHiddenLayer();

            NeuralNet::Layer& GetInputLayer();
            NeuralNet::OutputLayer& GetOutputLayer();

            void ForwardPropagation();
            void BackwardPropagation();
            double CalculateError(int expectedValue);

            int GetEpochs();
            int GetIterations();

            void SetEpochs(int epochs);
            void SetIterations(int iterations);

            int GetNumCorrect();
            void SetNumCorrect(int numCorrect);

            void UpdateWeights();

        private:
            NeuralNet::InputLayer _inputLayer;
            NeuralNet::OutputLayer _outputLayer;

            vector<NeuralNet::HiddenLayer*> _hiddenLayers;

            int _epochs;
            int _iterations;
            int _numCorrect;
    };
}
