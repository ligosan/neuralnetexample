#include "Utils.h"

using namespace std;

#ifndef LAYER
#define LAYER

namespace NeuralNet {
    class Layer {
        public:
            Layer(int numberOfInputNeurons, int numberOfNeurons);

            // Compute Functions
            void ActivationFunction(vector<double>& inputValues);
            void UpdateWeights();

            // Get
            int GetNumberOfNeurons();
            int GetNumberOfInputNeurons();

            double GetLearningRate();

            double GetWeightByIndex(int index);
            double GetBiaseByIndex(int index);
            double GetActivationByIndex(int index);

            vector<double>& GetWeight();
            vector<double>& GetActivation();
            vector<double>& GetBiases();
            vector<double>& GetDelta();

            // Set
            void SetLearningRate(double learningRate);

            void SetNumberOfNeurons(int numberOfNeurons);
            void SetNumberOfInputNeurons(int numberOfInputNeurons);

            void SetWeightByIndex(int index, double weight);
            void SetWeight(vector<double>& weights);

            void SetBiases(vector<double>& biases);
            void SetBiaseByIndex(int index, double bias);

            void SetActivation(vector<double>& value);
            void SetActivationByIndex(int index, double value);

        protected:
            vector<double> _activation; // Output
            vector<double> _weights;
            vector<double> _biases;
            vector<double> _deltas;

            vector<double> _newWeights;
            vector<double> _newBiases;

            int _numberOfNeurons;
            int _numberOfInputNeurons;

            double _learningRate = 0.003;
    };
}

#endif
