#include <vector>
#include <cmath>

#include "Layer.h"

using namespace std;

#ifndef INPUT_LAYER
#define INPUT_LAYER

namespace NeuralNet {
    class InputLayer : public Layer {
        public:
            InputLayer(int numberOfNeurons);

            vector<double>& GetValues();

            void SetValues(vector<double>& values);
            void SetValueByIndex(int index, double value);

        private:
            vector<double> _inputValues;
    };
}

#endif
