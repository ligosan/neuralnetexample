#include "HiddenLayer.h"

#include <iostream>

NeuralNet::HiddenLayer::HiddenLayer(int numberOfInputNeurons, int numberOfNeurons)
    : Layer(numberOfInputNeurons, numberOfNeurons){

}

void NeuralNet::HiddenLayer::BackPropagation(NeuralNet::Layer& outputLayer, NeuralNet::Layer& inputLayer) {
    for(int j = 0; j < this->_numberOfNeurons; j++) {
        double sumDeriv = 0;
        for(int k = 0; k < outputLayer.GetNumberOfNeurons(); k++) {
            auto delta = outputLayer.GetDelta()[k];
            auto weight = outputLayer.GetWeight()[k * outputLayer.GetNumberOfInputNeurons() + j];
            auto sigmoidDeriv = this->_activation[j] * (1 - this->_activation[j]);

            auto chainDeriv = delta * weight * sigmoidDeriv;
            sumDeriv += chainDeriv;
        }

        this->_deltas[j] = sumDeriv;

        for(int l = 0; l < this->_numberOfInputNeurons; l++) {
            auto totalChange = sumDeriv * inputLayer.GetActivation()[l];
            auto idx = j * this->_numberOfInputNeurons + l;
            this->_newWeights[idx] = this->_weights[idx] - this->_learningRate * totalChange;
        }

        // Biases
        auto netWeightDerivativeBias = 1;

        auto totalChangeBias = sumDeriv * netWeightDerivativeBias;
        this->_newBiases[j] = this->_biases[j] - this->_learningRate * totalChangeBias;
    }
}