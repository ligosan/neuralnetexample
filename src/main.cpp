#include "Layer.h"

#include "HiddenLayer.h"
#include "OutputLayer.h"

#include "NeuralNetwork.h"

#include "Utils.h"

std::vector<unsigned char> LoadImages(int& numberOfImages) {
    std::ifstream myFile;

    myFile.open("train-images-idx3-ubyte", ios::in | ios::binary | ios::ate);

    if(!myFile) {
        throw runtime_error("training data cannot be found");
    }
    
    myFile.seekg(0, std::ios::end);
    std::streampos fileSize = myFile.tellg();
    myFile.seekg(0, std::ios::beg);
    std::vector<unsigned char> fileData(fileSize);
    myFile.read((char*)&fileData[0], fileSize);

    int magicNumber = 0;
    magicNumber |= (((int)(fileData[0])) << 24);
    magicNumber |= (((int)(fileData[1])) << 16);
    magicNumber |= (((int)(fileData[2])) << 8);
    magicNumber |= (int)(fileData[3]);

    // std::cout << magicNumber << std::endl;

    numberOfImages = 0;
    numberOfImages |= (((int)(fileData[4])) << 24);
    numberOfImages |= (((int)(fileData[5])) << 16);
    numberOfImages |= (((int)(fileData[6])) << 8);
    numberOfImages |= (int)(fileData[7]);

    // std::cout << numberOfImages << std::endl;

    int numberOfRows = 0;
    numberOfRows |= (((int)(fileData[8])) << 24);
    numberOfRows |= (((int)(fileData[9])) << 16);
    numberOfRows |= (((int)(fileData[10])) << 8);
    numberOfRows |= (int)(fileData[11]);

    // std::cout << numberOfRows << std::endl;

    int numberOfColumns = 0;
    numberOfColumns |= (((int)(fileData[12])) << 24);
    numberOfColumns |= (((int)(fileData[13])) << 16);
    numberOfColumns |= (((int)(fileData[14])) << 8);
    numberOfColumns |= (int)(fileData[15]);

    // std::cout << numberOfColumns << std::endl;

    fileData.erase(fileData.begin(), fileData.begin() + 16);
    return fileData;
}

std::vector<unsigned char> LoadLabels(int& numberOfItems) {
    std::ifstream myFile;
    myFile.open("train-labels-idx1-ubyte", ios::in | ios::binary | ios::ate);
    if(!myFile) {
        throw runtime_error("training labels cannot be found");
    }
        myFile.seekg(0, std::ios::end);
        std::streampos fileSize = myFile.tellg();
        myFile.seekg(0, std::ios::beg);
        std::vector<unsigned char> fileData(fileSize);
        myFile.read((char*)&fileData[0], fileSize);
    
        int magicNumber = 0;
        magicNumber |= (((int)(fileData[0])) << 24);
        magicNumber |= (((int)(fileData[1])) << 16);
        magicNumber |= (((int)(fileData[2])) << 8);
        magicNumber |= (int)(fileData[3]);

        // std::cout << magicNumber << std::endl;

        numberOfItems = 0;
        numberOfItems |= (((int)(fileData[4])) << 24);
        numberOfItems |= (((int)(fileData[5])) << 16);
        numberOfItems |= (((int)(fileData[6])) << 8);
        numberOfItems |= (int)(fileData[7]);

        // std::cout << numberOfItems << std::endl;

        // for(int i = 8; i < fileData.size(); i++) {
        //     std::cout << (int)(fileData[i]) << std::endl;
        // }
        
    fileData.erase(fileData.begin(), fileData.begin() + 8);

    return fileData;
}

int main(int argc, char** argv) {

    int numberOfLabels;
    int numberOfImages;

    auto labelVector = LoadLabels(numberOfLabels);
    auto imageVector = LoadImages(numberOfImages);

    int pixels = 28 * 28;

    std::vector<double> initialValues;

    // NeuralNet::InputLayer inputLayer(pixels);

    NeuralNet::InputLayer inputLayer(pixels);
    NeuralNet::HiddenLayer hiddenLayer1(pixels, 128);
    NeuralNet::HiddenLayer hiddenLayer2(128, 128);
    NeuralNet::OutputLayer outputLayer(128, 10);

    NeuralNetwork::NeuralNetwork neuralNetwork(inputLayer, outputLayer);
    neuralNetwork.AddHiddenLayer(hiddenLayer1);
    neuralNetwork.AddHiddenLayer(hiddenLayer2);

    for(int i = 0; i < 3; i++) {
        NeuralNet::HiddenLayer* hiddenLayer = new NeuralNet::HiddenLayer(128, 128);
        neuralNetwork.AddHiddenLayer(*hiddenLayer);
    }

    neuralNetwork.GenerateRandomWeights();

    int iteration = 0;
    int epochs = 0;
    int numCorrect = 0;
    double accuracy = 0;

    neuralNetwork.LoadWeights();
    epochs = neuralNetwork.GetEpochs();
    iteration = neuralNetwork.GetIterations();
    numCorrect = neuralNetwork.GetNumCorrect();

    while(true) {
        for(int i = 0; i < numberOfImages; i++) {
            int desiredOutput = (int)labelVector[i];

            initialValues.clear();
            int start = i * pixels;
            for(int i = start; i < start + pixels; i++) {
                initialValues.push_back(imageVector[i]);
            }

            neuralNetwork.LoadInputValues(initialValues);
            neuralNetwork.ForwardPropagation();
            auto error = neuralNetwork.CalculateError(desiredOutput);
            neuralNetwork.BackwardPropagation();
            neuralNetwork.UpdateWeights();

            numCorrect += (desiredOutput == neuralNetwork.GetOutputLayer().GetHighestConfidence()) ? 1 : 0;
            accuracy = ((double)numCorrect / iteration) * 100;

            if(iteration % 5000 == 0) {
                // for(int i = 0; i < 28; i++) {
                //     for(int j = 0; j < 28; j++) {
                //         int pVal = initialValues[i * 28 + j] == 0 ? 0 : 1;
                //         printf("%d ", pVal);
                //     }
                //     printf("\n");
                printf("Epochs: %d  Iteration: %d  Expected Value: %d  Calculated Value: %d    Loss: %f     Accuracy: %f\n", epochs, iteration, desiredOutput, neuralNetwork.GetOutputLayer().GetHighestConfidence(), error, accuracy);
            }

            
            iteration++;
        }

        epochs++;

        neuralNetwork.SetEpochs(epochs);
        neuralNetwork.SetIterations(iteration);
        neuralNetwork.SetNumCorrect(numCorrect);
        neuralNetwork.SaveWeights();
    }
}