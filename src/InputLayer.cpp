#include "InputLayer.h"

NeuralNet::InputLayer::InputLayer(int numberOfNeurons) 
: NeuralNet::Layer::Layer(1, numberOfNeurons){
    
    this->_numberOfNeurons = numberOfNeurons;
    this->_inputValues.resize(numberOfNeurons);
}

void NeuralNet::InputLayer::SetValues(vector<double>& values) {
    this->_inputValues = values;
    this->_activation = values;
}

void NeuralNet::InputLayer::SetValueByIndex(int index, double value) {
    this->_inputValues[index] = value;
    this->_activation[index] = value;
}

vector<double>& NeuralNet::InputLayer::GetValues() {
    return this->_inputValues;
}