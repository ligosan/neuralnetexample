#include "Layer.h"
#include <iostream>

NeuralNet::Layer::Layer(int numberOfInputNeurons, int numberOfNeurons) {
    this->_numberOfNeurons = numberOfNeurons;
    this->_numberOfInputNeurons = numberOfInputNeurons;

    this->_weights.resize(numberOfInputNeurons * numberOfNeurons);
    this->_biases.resize(numberOfNeurons);
    this->_activation.resize(numberOfNeurons);
    this->_deltas.resize(numberOfNeurons);

    this->_newWeights.resize(numberOfInputNeurons * numberOfNeurons);
    this->_newBiases.resize(numberOfNeurons);

    std::fill(this->_biases.begin(), this->_biases.end(), 0);
}

void NeuralNet::Layer::SetBiaseByIndex(int index, double bias) {
    this->_biases[index] = bias;
}

void NeuralNet::Layer::ActivationFunction(vector<double>& inputValues) {
    if(_numberOfInputNeurons != inputValues.size()) {
        throw std::runtime_error("input value count did not match number of input neurons");
    }

    for(int i = 0; i < _numberOfNeurons; i++) {
        double weightedSum = 0;

        for(int j = 0; j < _numberOfInputNeurons; j++) {
            double inputValue = inputValues[j];
            // std::cout << inputValues.size() << std::endl;
            double weight = this->_weights[_numberOfInputNeurons * i + j];

            double product = inputValue * weight;
            weightedSum += product;

            // printf("inputValue: %f weight: %f product: %f weightedSum: %f\n", inputValue, weight, product, weightedSum);
        }
        
        weightedSum += this->_biases[i];
       
        // Sigmoid function
        double sigmoid = 1 / (1 + std::exp(-1 * weightedSum));
        this->_activation[i] = sigmoid;

        // std::cout << "ACTI " << this->_activation[i] << std::endl;
    }
}

int NeuralNet::Layer::GetNumberOfInputNeurons() {
    return this->_numberOfInputNeurons;
}

int NeuralNet::Layer::GetNumberOfNeurons() {
    return this->_numberOfNeurons;
}

double NeuralNet::Layer::GetWeightByIndex(int index) {
    return this->_weights[index];
}

vector<double>& NeuralNet::Layer::GetWeight() {
    return this->_weights;
}

void NeuralNet::Layer::SetNumberOfInputNeurons(int numberOfInputNeurons) {
    this->_numberOfInputNeurons = numberOfInputNeurons;
}

void NeuralNet::Layer::SetNumberOfNeurons(int numberOfNeurons) {
    this->_numberOfNeurons = numberOfNeurons;
}

void NeuralNet::Layer::SetWeightByIndex(int index, double weight) {
    this->_weights[index] = weight;
}

void NeuralNet::Layer::SetWeight(vector<double>& weights) {
    this->_weights = weights;
}

vector<double>& NeuralNet::Layer::GetActivation() {
    return this->_activation;
}

vector<double>& NeuralNet::Layer::GetBiases() {
    return this->_biases;
}

void NeuralNet::Layer::UpdateWeights() {
    // this->_weights = this->_newWeights;

    std::copy(this->_newWeights.begin(), this->_newWeights.end(), this->_weights.begin());
    std::copy(this->_newBiases.begin(), this->_newBiases.end(), this->_biases.begin());
}

double NeuralNet::Layer::GetLearningRate() {
    return this->_learningRate;
}

vector<double>& NeuralNet::Layer::GetDelta() {
    return this->_deltas;
}

