#include <algorithm>
#include <iostream>
#include <random>

#include "NeuralNetwork.h"

// void Train();
// void Inference();

void NeuralNetwork::NeuralNetwork::LoadWeights() {
    union DoubleValue {
        double doubleValue;
        char asChars[sizeof(double)];
    };

    union IntValue {
        int intValue;
        char asChars[sizeof(int)];
    };

    union DoubleValue doubleVal;
    union IntValue intVal;

    ifstream fileStream;

    fileStream.open("model_weights.dat", ios::in | ios::binary);

    if(!fileStream.is_open()) {
       return; 
    }

    fileStream.read(intVal.asChars, sizeof(int));
    this->_epochs = intVal.intValue;

    fileStream.read(intVal.asChars, sizeof(int));
    this->_iterations = intVal.intValue;

    fileStream.read(intVal.asChars, sizeof(int));
    this->_numCorrect = intVal.intValue;

    fileStream.read(intVal.asChars, sizeof(int));
    auto inputLayerNeurons = intVal.intValue;

    this->_inputLayer.SetNumberOfNeurons(inputLayerNeurons);

    fileStream.read(intVal.asChars, sizeof(int));
    auto numberOfHiddenLayers = intVal.intValue;
    for(int i = 0; i < numberOfHiddenLayers; i++) {
        fileStream.read(intVal.asChars, sizeof(int));
        auto inputNeurons = intVal.intValue;

        fileStream.read(intVal.asChars, sizeof(int));
        auto numNeurons = intVal.intValue;

        this->_hiddenLayers[i]->SetNumberOfInputNeurons(inputNeurons);
        this->_hiddenLayers[i]->SetNumberOfNeurons(numNeurons);

        // Biases
        for(int k = 0; k < numNeurons; k++) {
            fileStream.read(doubleVal.asChars, sizeof(double));
            this->_hiddenLayers[i]->SetBiaseByIndex(k, doubleVal.doubleValue);
        }

        for(int k = 0; k < (numNeurons * inputNeurons); k++) {
            fileStream.read(doubleVal.asChars, sizeof(double));
            this->_hiddenLayers[i]->SetWeightByIndex(k, doubleVal.doubleValue);
        }
    }

    fileStream.read(intVal.asChars, sizeof(int));
    auto inputNeurons = intVal.intValue;

    fileStream.read(intVal.asChars, sizeof(int));
    auto numNeurons = intVal.intValue;

    this->_outputLayer.SetNumberOfInputNeurons(inputNeurons);
    this->_outputLayer.SetNumberOfNeurons(numNeurons);

    // Biases
    for(int k = 0; k < numNeurons; k++) {
        fileStream.read(doubleVal.asChars, sizeof(double));
        this->_outputLayer.SetBiaseByIndex(k, doubleVal.doubleValue);
    }

    for(int k = 0; k < (numNeurons * inputNeurons); k++) {
        fileStream.read(doubleVal.asChars, sizeof(double));
        this->_outputLayer.SetWeightByIndex(k, doubleVal.doubleValue);
    }

    fileStream.close();
}

void NeuralNetwork::NeuralNetwork::SaveWeights() {
    union DoubleValue {
        double doubleValue;
        char asChars[sizeof(double)];
    };

    union IntValue {
        int intValue;
        char asChars[sizeof(int)];
    };

    union DoubleValue doubleVal;
    union IntValue intVal;

    ofstream fileStream;
    fileStream.open("model_weights.dat", ios::out | ios::binary);

    intVal.intValue = this->_epochs;
    fileStream.write(intVal.asChars, sizeof(int));

    intVal.intValue = this->_iterations;
    fileStream.write(intVal.asChars, sizeof(int));

    intVal.intValue = this->_numCorrect;
    fileStream.write(intVal.asChars, sizeof(int));

    intVal.intValue = this->_inputLayer.GetNumberOfNeurons();
    fileStream.write(intVal.asChars, sizeof(int));

    // Hidden Layer Count
    intVal.intValue = this->_hiddenLayers.size();
    fileStream.write(intVal.asChars, sizeof(int));

    // Hidden Layer
    for(auto layer : this->_hiddenLayers) {
        intVal.intValue = layer->GetNumberOfInputNeurons();
        fileStream.write(intVal.asChars, sizeof(int));

        intVal.intValue = layer->GetNumberOfNeurons();
        fileStream.write(intVal.asChars, sizeof(int));

        // Biases
        auto& biases = layer->GetBiases();
        for(auto bias : biases) {
            doubleVal.doubleValue = bias;
            fileStream.write(doubleVal.asChars, sizeof(double));
        }

        // Weights
        auto& weights = layer->GetWeight();
        for(auto weight : weights) {
            doubleVal.doubleValue = weight;
            fileStream.write(doubleVal.asChars, sizeof(double));
        }
    }

    // Output Layer
    intVal.intValue = this->_outputLayer.GetNumberOfInputNeurons();
    fileStream.write(intVal.asChars, sizeof(int));

    intVal.intValue = this->_outputLayer.GetNumberOfNeurons();
    fileStream.write(intVal.asChars, sizeof(int));

    // Biases
    auto& biases = this->_outputLayer.GetBiases();
    for(auto bias : biases) {
        doubleVal.doubleValue = bias;
        fileStream.write(doubleVal.asChars, sizeof(double));
    }

    // Weights
    auto& weights = this->_outputLayer.GetWeight();
    for(auto weight : weights) {
        doubleVal.doubleValue = weight;
        fileStream.write(doubleVal.asChars, sizeof(double));
    }

    fileStream.close();
}

NeuralNetwork::NeuralNetwork::NeuralNetwork(NeuralNet::InputLayer& inputLayer, NeuralNet::OutputLayer& outputLayer) 
    : _inputLayer(inputLayer), _outputLayer(outputLayer) {
        this->_epochs = 0;
        this->_iterations = 0;
        this->_numCorrect = 0;
}

NeuralNetwork::NeuralNetwork::NeuralNetwork() 
    : _inputLayer(0), _outputLayer(0, 0) {
        this->_epochs = 0;
        this->_iterations = 0;
        this->_numCorrect = 0;
}

void NeuralNetwork::NeuralNetwork::AddHiddenLayer(NeuralNet::HiddenLayer& hiddenLayer) {
    this->_hiddenLayers.push_back(&hiddenLayer);
}

vector<NeuralNet::HiddenLayer*>& NeuralNetwork::NeuralNetwork::GetHiddenLayer() {
    return this->_hiddenLayers;
}


NeuralNet::Layer& NeuralNetwork::NeuralNetwork::GetInputLayer() {
    return this->_inputLayer;
}

NeuralNet::OutputLayer& NeuralNetwork::NeuralNetwork::GetOutputLayer() {
    return this->_outputLayer;
}

void NeuralNetwork::NeuralNetwork::BackwardPropagation() {
    this->_outputLayer.BackwardPropagation(this->_hiddenLayers[this->_hiddenLayers.size() - 1]->GetActivation());

    if(this->_hiddenLayers.size() >= 2) {
        this->_hiddenLayers[this->_hiddenLayers.size() - 1]->BackPropagation(this->_outputLayer, *this->_hiddenLayers[this->_hiddenLayers.size() - 2]);
        auto& prevLayer = *this->_hiddenLayers[this->_hiddenLayers.size() - 1];
        for(int i = this->_hiddenLayers.size() - 2; i > 0; i--) {
            this->_hiddenLayers[i]->BackPropagation(prevLayer, *this->_hiddenLayers[i - 1]);
            prevLayer = *this->_hiddenLayers[i];
        }

        this->_hiddenLayers[0]->BackPropagation(*this->_hiddenLayers[1], this->_inputLayer);
    } else {
        this->_hiddenLayers[0]->BackPropagation(this->_outputLayer, this->_inputLayer);
    }
}

void NeuralNetwork::NeuralNetwork::ForwardPropagation() {
    // Hidden Layers
    this->_hiddenLayers[0]->ActivationFunction(this->_inputLayer.GetActivation());
    for(int i = 1; i < this->_hiddenLayers.size(); i++) {
        this->_hiddenLayers[i]->ActivationFunction(this->_hiddenLayers[i - 1]->GetActivation());
    }
    
    // Output Layer
    this->_outputLayer.ActivationFunction(this->_hiddenLayers[this->_hiddenLayers.size() - 1]->GetActivation());
}

void NeuralNetwork::NeuralNetwork::LoadInputValues(vector<double>& inputValues) {
    if(inputValues.size() != this->_inputLayer.GetNumberOfNeurons()) {
        // TODO throw exception
        throw std::runtime_error("input value count did not match number of input neurons");
    }

    for(int i = 0; i < this->_inputLayer.GetNumberOfNeurons(); i++) {
        auto normalized = (double)inputValues[i] / 255;
        this->_inputLayer.SetValueByIndex(i, normalized);
    }
}

double NeuralNetwork::NeuralNetwork::CalculateError(int expectedValue) {
    return this->_outputLayer.CalculateError(expectedValue);
}

void NeuralNetwork::NeuralNetwork::GenerateRandomWeights() {
    // TODO use DI
    std::random_device rd;
    std::default_random_engine engine(rd());

    for(auto hiddenLayer : this->_hiddenLayers) {
        double optimalValue = 1/sqrt(hiddenLayer->GetNumberOfInputNeurons());
        std::uniform_real_distribution<double> layerRandom((-1 * optimalValue), optimalValue);

        for(int i = 0; i < hiddenLayer->GetNumberOfNeurons(); i++) {
            for(int j = 0; j < hiddenLayer->GetNumberOfInputNeurons(); j++) {
                hiddenLayer->SetWeightByIndex((hiddenLayer->GetNumberOfInputNeurons() * i + j), layerRandom(engine));
            }
            // hiddenLayer->SetBiaseByIndex(i, layerRandom(engine));
        }
    }

    double optimalValue = 1/sqrt(this->_outputLayer.GetNumberOfInputNeurons());
    std::uniform_real_distribution<double> layerRandom((-1 * optimalValue), optimalValue);
    for(int i = 0; i < this->_outputLayer.GetNumberOfNeurons(); i++) {
        for(int j = 0; j < this->_outputLayer.GetNumberOfInputNeurons(); j++) {
            this->_outputLayer.SetWeightByIndex((_outputLayer.GetNumberOfInputNeurons() * i + j), layerRandom(engine));
        }
        // this->_outputLayer.SetBiaseByIndex(i, layerRandom(engine));
    }
}

void NeuralNetwork::NeuralNetwork::UpdateWeights() {
    this->_outputLayer.UpdateWeights();
    
    for(auto layer : this->_hiddenLayers) {
        layer->UpdateWeights();
    }
}

void NeuralNetwork::NeuralNetwork::SetEpochs(int epochs) {
    this->_epochs = epochs;
}

void NeuralNetwork::NeuralNetwork::SetIterations(int iterations) {
    this->_iterations = iterations;
}

void NeuralNetwork::NeuralNetwork::SetNumCorrect(int numCorrect) {
    this->_numCorrect = numCorrect;
}

int NeuralNetwork::NeuralNetwork::GetEpochs() {
    return this->_epochs;
}

int NeuralNetwork::NeuralNetwork::GetIterations() {
    return this->_iterations;
}

int NeuralNetwork::NeuralNetwork::GetNumCorrect() {
    return this->_numCorrect;
}