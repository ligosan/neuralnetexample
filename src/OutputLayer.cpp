#include "OutputLayer.h"

#include <algorithm>
#include <iostream>


NeuralNet::OutputLayer::OutputLayer(int numberOfInputNeurons, int numberOfNeurons) 
    : NeuralNet::Layer::Layer(numberOfInputNeurons, numberOfNeurons) {
    this->_outputValues.resize(numberOfNeurons);
    for(int i = 0; i < numberOfNeurons; i++) {
        this->_outputValues[i] = i;
    }
}

int NeuralNet::OutputLayer::GetHighestConfidence() {
    auto it = std::max_element(this->GetActivation().begin(), this->GetActivation().end());
    auto maximumConfidence = this->_outputValues[std::distance(this->_activation.begin(), it)];
    return maximumConfidence;
}


vector<int>& NeuralNet::OutputLayer::GetOutputValues() {
    return this->_outputValues;
}

double NeuralNet::OutputLayer::CalculateError(int expectedOutput) {
    this->_desiredOutput = expectedOutput;
    double error = 0;
    
    for(int i = 0; i < this->_activation.size(); i++) {
        auto addVal = i == expectedOutput ? 1 : 0;
        error += pow((this->_activation[i] - addVal), 2);
    }

    error /= this->_activation.size();

    // Cross entropy
    // for(int i = 0; i < this->_activation.size(); i++) {
    //     auto addVal = i == expectedOutput ? 1 : 0;
    //     error += -1 * addVal * log(this->_activation[i]);
    // }

    // std::cout << error << std::endl;

    return error;
}

int NeuralNet::OutputLayer::GetDesiredOutput() {
    return this->_desiredOutput;
}

void NeuralNet::OutputLayer::BackwardPropagation(vector<double>& inputValues) {
    if(this->_numberOfInputNeurons != inputValues.size()) {
        throw runtime_error("number of input neuron counts dont match");
    }

    for(int i = 0; i < this->GetNumberOfNeurons(); i++) {
         // Output Layer
        auto expectedVal = i == this->_desiredOutput ? 1 : 0;

        auto costActivationDerivative = 2 * (this->GetActivation()[i] - expectedVal);
        auto activationSigmoidDerivative = this->GetActivation()[i] * (1 - this->GetActivation()[i]);
        auto delta = costActivationDerivative * activationSigmoidDerivative;

        this->_deltas[i] = delta;

        for(int j = 0; j < this->_numberOfInputNeurons; j++) {
            // Chain rule
            auto totalChange = delta * inputValues[j];
            auto idx = this->_numberOfInputNeurons * i + j;
            this->_newWeights[idx] = this->_weights[idx] - this->_learningRate * totalChange;
        }

        // Biases
        auto netWeightDerivativeBias = 1;

        auto totalChangeBias = delta * netWeightDerivativeBias;
        this->_newBiases[i] = this->_biases[i] - this->_learningRate * totalChangeBias;
    }
}
