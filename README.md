# Neural Net Example #

This repository contains code that was written to distinguish hand written numbers using a classic neural network. 
It was written purely for educational purposes only.

The [MNIST dataset](https://deepai.org/dataset/mnist) was used for both training and inferencing.

The below screenshot shows a model with a single hidden layer, with 128 neurons being trained for 50 epochs, having accuracy upto 80%.

![Training Stats](docs/images/TrainingStats.png)

### TODO ###
---
 * Batching, currently only single batch learning support :(
 * CUDA port for matrix multiplications
 * Different loss functions (e.g. categorical cross entropy maybe?)


### Owner ###
---
Michael Jang