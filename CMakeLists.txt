cmake_minimum_required(VERSION 3.16.3)
project(NeuralNetExample)

FILE(GLOB LayerSourceFiles ${PROJECT_SOURCE_DIR}/src/*.cpp)
set(BINARY_NAME neural_net_example)

add_executable(${BINARY_NAME})
set_target_properties(${BINARY_NAME} PROPERTIES LINKER_LANGUAGE CXX)
target_include_directories(${BINARY_NAME} PRIVATE ${PROJECT_SOURCE_DIR}/include)
target_sources(${BINARY_NAME} PRIVATE 
                ${PROJECT_SOURCE_DIR}/src/main.cpp
                ${LayerSourceFiles})
add_custom_command(TARGET ${BINARY_NAME} POST_BUILD 
                    COMMAND ${CMAKE_COMMAND} -E copy_directory 
                    ${PROJECT_SOURCE_DIR}/dataset 
                    ${PROJECT_SOURCE_DIR}/bin)